<?php
/**
 * Created by PhpStorm.
 * User: pzzrudlf
 * Date: 2017/10/4 0004
 * Time: 18:05
 */

namespace api\common\controllers;


class SiteController extends \yii\web\Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}