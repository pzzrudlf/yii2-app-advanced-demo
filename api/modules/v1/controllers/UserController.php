<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\web\Response;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\ArrayHelper;

class UserController extends ActiveController
{
	public $modelClass = 'api\common\models\User';

	public function behaviors()
	{
		// $behaviors = parent::behaviors();
		// $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
		// return $behaviors;
		return ArrayHelper::merge(parent::behaviors(), [
			'authenticator' => [
				'class' => HttpBearerAuth::className(),
				'optional' => [
					'login',
					'signup',
					'index',
				],
			],
			// 'corsFilter' => [
			// 	'class' => \yii\filters\Cors::className(),
			// 	'cors' => [
			// 		'Origin' => ['*'],
			// 		'Access-Control-Request-Headers' => ['authorization'],
			// 	],
			// ],
			// 'contentNegotiator' => [
			// 	'formats' => [
			// 		'text/html' => Response::FORMAT_JSON,
			// 	],
			// ],
		]);
	}

	public function actionSignup()
	{
		return 'signup-test';
	}

}
