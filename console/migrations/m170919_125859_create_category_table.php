<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m170919_125859_create_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey()->notNull()->comment('分类id'),
            'title' => $this->string(50)->comment('分类名字'),
            'pid' => $this->integer(11)->notNull()->defaultValue(0)->comment('父级分类id'),
            'desc' => $this->string(1000)->comment('分类描述'),
            'sort' => $this->integer(11)->defaultValue(0)->comment('分类排行'),
            'created_at' => $this->dateTime()->comment('创建时间'),
            'updated_at' => $this->dateTime()->comment('更新时间'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('category');
    }
}
