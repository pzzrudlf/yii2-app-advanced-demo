<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m170919_125809_create_post_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('post', [
            'id' => $this->primaryKey()->notNull()->comment('主键id'),
            'user_id' => $this->integer(11)->notNull()->comment('作者id'),
            'category_id' => $this->integer()->defaultValue(1)->comment('文章分类id'),
            'title' => $this->string()->comment('文章标题'),
            'content' => $this->text()->comment('文章内容'),
            'tags' => $this->text()->comment('文章标签'),
            'status' => $this->integer(11)->notNull()->comment('文章状态'),
            'created_at' => $this->dateTime()->comment('创建时间'),
            'updated_at' => $this->dateTime()->comment('更新时间'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('post');
    }
}
