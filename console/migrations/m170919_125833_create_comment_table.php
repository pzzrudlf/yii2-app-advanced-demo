<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comment`.
 */
class m170919_125833_create_comment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('comment', [
            'id' => $this->primaryKey()->notNull()->comment('主键id'),
            'user_id' => $this->integer(11)->comment('评论者id'),
            'post_id' => $this->integer(11)->comment('文章id'),
            'parent_id' => $this->integer(11)->comment('父级评论id'),
            'reply_id' => $this->integer(11)->comment('回复的评论id'),
            'content' => $this->text()->notNull()->comment('评论内容'),
            'status' => $this->integer(11)->defaultValue(0)->comment('评论状态'),
            'created_at' => $this->dateTime()->comment('创建时间'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('comment');
    }
}
