<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tag`.
 */
class m170919_125843_create_tag_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tag', [
            'id' => $this->primaryKey()->notNull()->comment('文章标签id'),
            'name' => $this->string(128)->notNull()->comment('文章标签名字'),
            'frequency' => $this->integer(11)->defaultValue(1)->comment('文章标签权重'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tag');
    }
}
